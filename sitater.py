import random

def sitat():

    sitater = ["""Til lags åt alle kan ingen gjera;
    det er no gamalt og vil so vera.
    Behage | Seg sjølv og andre""", 

    """D’er seint å stengja Buret, når Fuglen er flogen.
    Fugler""", 

    """Det ein sjølv kann gjera, skal ein ikkje bidja andre um.
    Gjøre | Seg sjølv og andre""", 

    """Gleda gløymest, fyrr du trur,
    men sorgerna sitja lenge.
    Glede og sorg
    Ein kann ikkje alltid spinna Silke.
    Godt nok""", 

    """Heimen er både vond og god;
    det skifter med sut og gaman.
    Hæv er den som med same mod
    kan taka mot alt i saman.
    Hjem""", 
    """ D’er bedre vera ukjend en illa kjend.
    Kjendis""", 

    """Kjært er livet med all sin harm;
    det klårnar, alt som det gjenger.
    Men endå treng eg ei von i barm
    om eitt som varer lenger.
    Livet etter dette""", 

    """Strenge dagar må koma på,
    men etter koma dei milde.
    Medgang og motgang | Dag
    D’er betre hava litet Ro en myket med Uro.
    Ro og uro""", 

    """Der eg ikkje slepp inn, der slepp eg ganga ut.
    Tilhørighet | Utenfor""", 
    """Nei, vesle Vitet det rekk ikkje til.
    Ei Tru maa stydja upp-under.
    Tro""", 

    """Ven er den vide jord i vårdags liten,
    då alt som kryr av nyvakt liv er fullt.
    Vår"""]
    
    quote = sitater[random.randint(0, len(sitater) - 1)]

    return quote
