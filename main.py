#!/usr/bin/env python3

import discord
# from dotenv import load_dotenv
import os
import json
import math
import re
from datetime import datetime
from dotenv import load_dotenv
from sitater import sitat
from random import choice
from read import YTDLSource
from dictionary import dictionary

class Server:
    def __init__(self, ID, name):
        self.count = 0
        self.name = name
        self.ID = ID
        self.users = dict()
        self.last_message_by = None
        self.log = Logger(self.name, self.ID)

    @property
    def leaderboard(self):
        sortable = dict()
        for user in self.users.values():
            sortable[user.ID] = user.exp
        leaderboard = dict(sorted(sortable.items(), key=lambda item: item[1], reverse=True))
        self.__leaderboard = [self.users[i] for i in leaderboard]
        return self.__leaderboard

    def disp_leaderboard(self):
        headder = "```Rank | User                | Level | Exp\n"
        newLine = "-----|---------------------|-------|--------------\n"

        finalString = ""
        rank = 0
        for user in self.leaderboard:
            usr_name = user.name
            rank += 1
            if rank == 10:
                break

            if len(usr_name) >= 18:
                usr_name = usr_name[0:18]

            lenName = (20 - (len(usr_name))) * " "
            lenLevel = (6 - (len(str(user.level)))) * " "
            name = " " + usr_name + lenName + "|"
            level = lenLevel + str(user.level) + " | "

            string = f"{rank}    |{name}{level}{user.exp}\n"
            finalString = finalString + string + newLine

        rammaga = headder + newLine + finalString + "```"
        return rammaga

    def __str__(self):
        res = ""
        for user in self.users.values():
            res += f"Name: {user.name}, ID: {user.ID}, Exp: {user.exp}\n"
        return res


class User:
    def __init__(self, ID, name, exp):
        self.name = name
        self.exp = exp
        self.ID = ID
        self.recent_messages = list()
        # self.level = 0,5*math.sqrt(self.exp)

    @property
    def level(self):
        abs_level = math.ceil((math.sqrt(2)/10)*math.sqrt(abs(self.exp)))
        if self.exp <= 0:
            self.__level = abs_level*-1
        else:
            self.__level = abs_level

        return self.__level

    def check_recent_message(self, message):
        if message in self.recent_messages:
            no_exp = True
        else:
            no_exp = False
        if len(self.recent_messages) >= 10:
            self.recent_messages = self.recent_messages[0:10]
        self.recent_messages.append(message)
        return no_exp

    def rank(self):
        if self.level > 0:
            req_exp = self.level**2 * 50
            last_level_exp = (self.level - 1)**2 * 50
            progress = int(((self.exp - last_level_exp) / (req_exp - last_level_exp)) * 20)
            progress_bar = ("#" * progress) + "-" * (20-progress)
            return (f"**{self.name}**\n"
                    f"Nynorsknivå: **{self.level}**\n"
                    f"Framgang til nivå **{self.level +1}:** ```[{progress_bar}] {self.exp-last_level_exp}/{req_exp-last_level_exp}```")

        return f"Du er nivå **{self.level}** i Nynorsk. Du bør ta seg saman!"

    def __str__(self):
        return f"Name: {self.name}, ID: {self.ID}, Exp: {self.exp}"


class Data:
    def __init__(self):
        self.servers = dict()

    def add_server(self, message):
        server_id = message.guild.id
        user_id = message.author.id
        if server_id not in self.servers:
            self.servers[server_id] = Server(server_id, message.guild.name)
            self.servers[server_id].users[user_id] = User(user_id, message.author.name, 10)
            return True
        return False

    def read_data(self):
        json_files = list()
        if not os.path.exists("./data"):
            os.mkdir("./data")
        for f in os.listdir("./data/"):
            if f.endswith(".json"):
                json_files.append(f)

        for i in json_files:
            path = f"./data/{i}"
            server = i.split(".")[0].split("_")
            server_ID = int(server[0])
            server_name = server[1]
            with open(path, "r") as f:
                tmp = json.load(f)
            self.servers[server_ID] = Server(server_ID, server_name)
            for key in tmp:
                # {key: User(tmp[key]["name"], tmp[key]["exp"])}
                self.servers[server_ID].users[int(key)] = User(int(key), tmp[key]["name"], tmp[key]["exp"])

    def dump_data(self, server):
        dump = dict()
        filename = f"./data/{server.ID}_{server.name}.json"
        print(server.users.values())
        for user in server.users.values():
            dump[user.ID] = {"name": user.name, "level": user.level, "exp": user.exp}
        print(dump)
        with open(filename, "w") as f:
            json.dump(dump, f, indent=4)
        server.log.save()

    def dump_all(self):
        for server in self.servers.values():
            self.dump_data(server)

    def __str__(self):
        res = ""
        for server in self.servers.values():
            res += f"Server: {server.name}, ID: {server.ID}\n"
            for user in server.users.values():
                res += f"Name: {user.name}, ID: {user.ID}, Exp: {user.exp}\n"
        return res


class Bot(discord.Client):
    man = """
    Her er ei liste med kommandoer:
    -     **-h**  eller **--hjelp** for help
    -     **!ivar <ord>** sjekker om eit ord er nynorsk
    -     **-s <ord>** eller **--suggest <ord>** for ordforslag
    -     **!ivar rank** for å sjå framgang til neste nivå
    -     **!ivar ranks** for oversikt over kven som tenar Noregs Mållag mest
    """

    def __init__(self, data):
        intents = discord.Intents.default()
        intents.message_content = True
        super().__init__(intents=intents)

        self.data = data
        self.exp_limit = 100

    async def on_ready(self):
        print(f"Bot joined as {self.user}")

    async def on_message(self, message):
        response = None
        if message.author.bot or message.content == "":
            return None

        if self.data.add_server(message):
            await message.channel.send(Bot.man)
        if message.author.id not in self.data.servers[message.guild.id].users:
            self.data.servers[message.guild.id].users[message.author.id] = User(message.author.id, message.author.name, 10)

        server = self.data.servers[message.guild.id]
        user = server.users[message.author.id]
        log = str()
        log += f"{datetime.now().strftime('%d-%m-%Y %H:%M')} by {user.name} in {message.channel.name}, EXP gain: "

        if message.content.split()[0].lower() == "!ivar":
            response = self.parse_args(message)

        # elif user.check_recent_message(message.content):
        #     user.recent_messages.append(message.content)
        #     return None
        # elif message.author.id == server.last_message_by:
        #     return None

        else:
            server.count += 1
            if server.count == 10:
                data.dump_data(server)
                server.count = 0
            server.last_message_by = message.author.id
            user.recent_messages.append(message.content)
            levelup, leveldown, exp = self.check_rank(message)
            log += str(exp)
            if levelup:
                response = f"Gratulerer du er no nivå: {user.level} i nynorsk. Du gjer ivar aasen stolt!\n```{sitat()}```"
            elif leveldown:
                response = f"Du set kniv i mitt nynorsk hjarte. Du er no nivå: {user.level} i nynorsk. Eit steg nærmare Danks statsborgerskap."

        if response is True:
            voiceChannel = await message.author.voice.channel.connect()
            player = await YTDLSource.from_url("https://www.youtube.com/watch?v=dYVbJnwV2NM", loop=bot.loop)
            voiceChannel.play(player, after=lambda e: bot.loop.create_task(voiceChannel.disconnect()))
            await message.channel.send('Eg les no: {}'.format(player.title))

        elif response is not None:
            await message.channel.send(response)

        server.log.add_to_buf(f"{log} Message: {message.content} \n")

    def parse_args(self, message):
        args = message.content.split()[1:]
        server_id = message.guild.id
        user_id = message.author.id
        if len(args) == 0:
            return Bot.man

        while args:
            arg = args.pop(0).lower()

            if arg == "rank":
                return self.data.servers[server_id].users[user_id].rank()
            elif arg == "ranks":
                return self.data.servers[server_id].disp_leaderboard()

            elif arg == "-s" or arg == "--suggest":
                try:
                    return self.get_suggestions(args.pop(0))
                except IndexError:
                    return Bot.man

            elif arg in ["-h", "--hjelp", "hjelp", "help"]:
                return Bot.man

            elif arg == "dikt":
                return self.dikt()

            elif arg == "les":
                return True

            else:
                return self.check_spelling_nn(arg)

    def calculate_exp(self, words):
        exp = 0
        words = re.sub(r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)", "", words)
        words = words.strip().split()

        for word in words:
            word = re.sub(r"[!?.]$", "", word)

            if re.match("[^a-zæøå]", word.lower()):
                continue

            spelling = dictionary.check_spelling(word)

            if spelling == dictionary.Spelling.NN:
                exp += 5
            elif spelling == dictionary.Spelling.BM:
                exp -= 15
            elif spelling == dictionary.Spelling.COMMON:
                exp += 1

        return exp

    def check_rank(self, message):
        user = self.data.servers[message.guild.id].users[message.author.id]
        rank_up = False
        rank_down = False
        exp_gain = self.calculate_exp(message.content)
        last_level = user.level
        if exp_gain > 100:
            exp_gain = 100

        user.exp += exp_gain
        if last_level < user.level:
            rank_up = True
        elif last_level > user.level:
            rank_down = True

        # print(f"Tot exp: {user.exp}, Exp gain: {exp_gain} User: {user.name}\nMessage: {message.content}")
        return rank_up, rank_down, exp_gain

    def get_suggestions(self, word):
        suggestions = dictionary.get_suggested_words_nn(word)

        if not suggestions:
            return f"Fann ingen liknande ord til **{word}**"

        suggestions = " | ".join(suggestions)
        if dictionary.check_spelling(word, "nn") == dictionary.Spelling.NN:
            return f"**{word}** er i mi Nynorskordbok. Her er nokre forslag til liknande ord:\n```{suggestions}```"
        else:
            return f"**{word}** er ikkje i mi Nynorskordbok. Her er nokre forslag til liknande ord:\n```{suggestions}```"

    def check_spelling_nn(self, word):
        if dictionary.check_spelling(word, "nn") == dictionary.Spelling.NN:
            return f"**{word}** er i mi nynorskordbok"
        else:
            return f"**{word}** er ikkje i mi nynorskordbok"


    def dikt(self):
        folder = choice(os.listdir("./dikt"))
        fil = choice(os.listdir("./dikt/" + folder))

        with open("./dikt/" + folder + "/" + fil, "r") as f:
            folder = folder.replace("_", " ")
            fil = fil.replace("_", " ")
            fromBook = f"Frå boka: {folder} \nDikt: {fil} \n"
            dikt = fromBook + "```" + f.read() + "```" + "    *Ivar Aasen*"

            return dikt


class Logger:
    def __init__(self, server_name, server_id):
        self.server_name = server_name
        self.server_id = server_id
        self.logfile = f"logs/{self.server_name}_{self.server_id}.log"
        self.buf = str()
        # Clear log on bot startup

        with open(self.logfile, "w") as f:
            f.write("")

    def save(self):
        with open(self.logfile, "a") as f:
            f.write(self.buf)
        self.buf = ""

    def add_to_buf(self, string):
        self.buf += string



if __name__ == "__main__":
    data = Data()
    data.read_data()
    load_dotenv()
    token = os.getenv("discordToken")
    bot = Bot(data)
    try:
        bot.run(token)
    finally:
        data.dump_all()
