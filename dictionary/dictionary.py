#!/usr/bin/env python3

import requests
from enum import Enum

class Spelling(Enum):
    BM = 1
    NN = 2
    COMMON = 3
    WRONG = 4

class UibInterface:
    def suggest(self, q, dictionary="nn,bm", word_class="", n=10, include="ei"):
        """Suggest search keywords"""
        dictionary = self.__get_dictionary(dictionary)
        self.__check_valid_wordclass(word_class)


        try:
            n = int(n)

        except ValueError:
            raise Exception(f"n, number of results to return must be of type integer")


        if not all(c in "eifs" for c in include):
            raise Exception(f"Invalid types of suggestions to be included in the response."
                            "valid incluedes are"
                            "e (exact lemma), f (full-text search), i (inflected forms), s (similar - fuzzy search)")

        url = f"https://ord.uib.no/api/suggest?q={q}&dict=bm%2Cnn&wc={word_class}&n=4&include=eis&dform=int"
        return requests.get(url).json()


    def article_search(self, q, dictionary="nn,bm", word_class="", scope="ei"):

        dictionary = self.__get_dictionary(dictionary)
        self.__check_valid_wordclass(word_class)

        if not all(c in "eif" for c in scope):
            raise Exception(f"Invalid types of suggestions to be included in the response."
                            "valid incluedes are"
                            "e (exact lemma), f (full-text search), i (inflected forms)")

        url = f"https://ord.uib.no/api/articles?w={q}&dict={dictionary}&wc={word_class}&scope={scope}"

        return requests.get(url).json()

    def fetch_article(self, article_id, dictionary="nn"):
        """Fetches an article based on article ID"""
        dictionary = self.__get_dictionary(dictionary)

        try:
            int(article_id)
        except ValueError:
            raise Exception("Article ID must be an integer")

        url = f"https://ord.uib.no/{dictionary}/article/{article_id}.json"

        return requests.get(url).json()

    def __get_dictionary(self, dictionary):

        if dictionary not in ("bm", "nn", "bm,nn", "nn,bm", 1, 2 ,3):
            raise Exception('Dictionary must be in: ("bm", "nn", "bm,nn", "nn,bm", 1, 2 ,3)')

        if dictionary == 1:
            dictionary = "bm"
        elif dictionary == 2:
            dictionary = "nn"

        elif dictionary == 3:
            dictionary = "bm%2Cnn"

        return dictionary

    def __check_valid_wordclass(self, wc):
        if wc not in ("ADJ", "ADP", "ADV", "AUX", "CCONJ", "DET", "INTJ", "NOUN", "NUM",
                            "PART", "PRON", "PROPN", "PUNCT", "SCONJ", "SYM", "VERB", "X", ""):
            raise Exception(f"{wc} is not a valid word class. Valid word classes are:"
                            f"ADJ, ADP, ADV, AUX, CCONJ, DET, INTJ, NOUN, NUM, PART, PRON, PROPN, PUNCT, SCONJ, SYM, VERB, X"
                            "Leave wordclass empty to include all wordclasses")


def check_spelling(word, dictionary="nn,bm"):
    res = uib.suggest(word, dictionary, include="e")["a"]
    print(Spelling.__members__)

    if "exact" not in res.keys():
        return Spelling.WRONG
    for i in res["exact"]:
        if i[0] == word:
            return Spelling(i[1])
        else:
            return Spelling.WRONG


def get_suggested_words_nn(word):
    res = uib.suggest(word, dictionary="nn")["a"]
    words = list()
    if "exact" in res.keys():
        for i in res["exact"]:
            if i[1] == Spelling.NN.value or i[1] == Spelling.COMMON.value:
                words.append(i[0])
    if "similar" in res.keys():
        for i in res["similar"]:
            if i[1] == Spelling.NN.value or i[1] == Spelling.COMMON.value:
                words.append(i[0])
    print(words)

    return words


def __suggest_to_json(word):
    mau = uib.suggest(word, "nn", include="e")
    import json
    with open("test.json", "w") as f:
        json.dump(mau, f)

uib = UibInterface()

if __name__ == "__main__":
    __suggest_to_json("hva")
    get_suggested_words_nn("hva")
